# Use the official MySQL image
FROM mysql

# Set the necessary environment variables
ENV MYSQL_ROOT_PASSWORD=root
ENV MYSQL_DATABASE=todo

# Copy the SQL file to the container
COPY ./database/todo.sql /docker-entrypoint-initdb.d/

# Expose the MySQL port (default is 3306)
EXPOSE 3306

# Additional instructions
