# Use Apache as the base image
FROM httpd:2.4

# Copy application files to the container
COPY ./dist/spa /usr/local/apache2/htdocs

# Expose the container port (default is 80 for Apache)
EXPOSE 80
